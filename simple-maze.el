;;; simple-maze --- Summary
;; Navigate the maze to the exit.

;;; Commentary:
;;
;; Emacs Quickstart:
;;   (require 'simple-maze)
;;   (sm/run)
;;
;; Controls:
;;  up/down/left/right arrows: Move
;;  >   : exit (perform on exit)
;;  q   : quit
;;  f5  : Save
;;  C-g : Exit, stay in window, cursor is hidden
;;

;;; Code:

;; generators

(defun sm/make-wall (x1 y1 x2 y2)
  "Return wall mapping starting at X1 Y1 to X2 Y2."
  (let ((wall ()))
    (dotimes (x (+ 1 (- x2 x1)))
      (dotimes (y (+ 1 (- y2 y1)))
        (push (cons (+ x1 x) (+ y1 y)) wall)))
    wall))

(defun sm/make-map (height width)
  "Create a map based on HEIGHT and WIDTH."
  (let ((real-height (1+ (* 2 height)))
        (real-width (1+ (* 2 width))))
    (list (cons real-width real-height)
          (sm/generate-walls width height))))

(defun sm/generate-walls (x y)
  "Return a map of walls based on dimensions X and Y."
 (let ((maze (sm/initalize-maze x y))
       (visited nil)
       (next-steps '((1 . 1)))
       (cell nil))
   (while (setq cell (pop next-steps))
     (let* ((x (car cell))
            (y (cdr cell)))
       (push (cons x y) visited)
       (loop for (u v) in (sm/neighbors x y)
             unless (member (cons u v) visited)
             do (let ()
                  (if (member (cons u v) maze)
                      (if next-steps
                          (push (cons u v) next-steps)
                        (setq next-steps (list (cons u v)))))
                  (if (sm/in-maze u v maze)
                      (let ()
                        (sm/delete-cell u v maze)
                        (sm/delete-cell (/ (+ x u) 2) (/ (+ y v) 2) maze))
                      )))
       (setq next-steps (sm/shuffle next-steps))))
   (sm/delete-cell 0 0 maze)
   maze))

(defun sm/shuffle (list)
  "Return a reordered version of LIST."
  (sort list (lambda (x y)
               (eq 0 (random 2)))))

(defun sm/in-maze (x y maze)
  "Check if cell at X Y is in the MAZE."
  (member (cons x y) maze))

(defun sm/delete-cell (x y maze)
  "Delete cell X Y from MAZE."
  ;; (if (member (cons x y) maze)
  ;;     (message "deleting: %s" (cons x y)))
  (delete (cons x y) maze))

(defun sm/initalize-maze (width height)
  "Initalize a map oof cells based on WIDTH and HEIGHT."
  (let ((maze nil)
        (height (1+ (* 2 height)))
        (width (1+ (* 2 width))))
    (dotimes (x width)
      (dotimes (y height)
        (setq maze (cons (cons (1+ x) (1+ y)) maze))))
    maze))

;; // ADD REAL MAX-X MAX-y
(defun sm/neighbors (x y)
  "Return neighbors to cell X Y."
  (remove-if-not
   (lambda (x-y) (and
                  (< -1 (first x-y))
                  (< -1 (second x-y))))
   (list (list x (+ y 2))
         (list (- x 2) y)
         (list x (- y 2))
         (list (+ x 2) y))))


(defun sm/save-state (state filepath)
  "Save the current STATE Lisp object to FILEPATH."
  (let ()
    (write-region (format "%s" state) nil filepath) t))

(defun sm/save-config (config filepath)
  "Save the current CONFIG Lisp object to FILEPATH."
  (let ()
    (write-region (format "%s" config) nil filepath) t))

(defun sm/load-state (filepath)
  "Return the contents of FILEPATH as a Lisp object."
  (let ((content nil))
    (if (file-exists-p filepath)
        (read (with-temp-buffer (insert-file-contents filepath)
                                (buffer-string))))))

(defun sm/load-map (filepath)
  "Return the contents of FILEPATH map.
Should return the same a sm/make-map."
  (let ((map-lines (with-temp-buffer (insert-file-contents filepath)
                                     (remove-if 'null (split-string (buffer-string) "\n" t))))
        (row-index 1)
        (col-index 1)
        (map-data ()))
    (let ((dimensions (cons (length (car map-lines)) (length map-lines)))
          (walls ()))
      (mapc (lambda (row)
              (progn
                (mapc (lambda (char) (let ()
                                       (if (char-equal char ?\#)
                                           (setq walls (cons (cons col-index row-index) walls)))
                                       (incf col-index)))
                      row)
                (incf row-index))
              (setq col-index 1))
            map-lines)
      (list dimensions walls))))

;; defvar

(defvar *STATE* nil "Contains game state.")
(defvar *CONFIG* (list (sm/make-map 5 10 ) (list ?\  ?\@ ?\>)) "Contains game configuration.")
(defvar screen-height (window-body-height) "The full display height to be used.")
(defvar screen-width  (window-body-height) "The full display width to be used.")

;; override defvars

;; ((map) (background-char player-char goal-char))
;; (setq *CONFIG* (list (sm/load-map "./data/level0.map") (list ?\. ?\@ ?\>)))
;((player-x player-y))
;; (setq *STATE* (sm/load-state "./data/game.dat"))
;(setq *STATE* nil)

(defun sm/map (config)
  "Return map from CONFIG."
  (car config))
(defun sm/map-walls (map)
  "Return map width from MAP."
  (cadr map))
(defun sm/map-width (map)
  "Return map width from MAP."
  (caar map))
(defun sm/map-height (map)
  "Return map height from MAP."
  (cdar map))

(defun sm/char-list (config)
  "Return the character list from CONFIG."
  (cadr config))
(defun sm/char-background (config)
  "Return background from CONFIG."
  (car (sm/char-list config)))
(defun sm/char-player (config)
  "Return player character from CONFIG."
  (cadr (sm/char-list config)))
(defun sm/char-goal (config)
  "Return goal character from CONFIG."
  (caddr (sm/char-list config)))

(defun sm/player-position (config)
  "Return player position from CONFIG."
  (car config))
(defun sm/player-x (config)
  "Return player x from CONFIG."
  (car (sm/player-position config)))
(defun sm/player-y (config)
  "Return player y from CONFIG."
  (cdr (sm/player-position config)))

(defun sm/goal-position (config)
  "Return goal position from CONFIG."
  (cadr config))
(defun sm/goal-x (config)
  "Return goal x from CONFIG."
  (car (sm/goal-position config)))
(defun sm/goal-y (config)
  "Return goal y from CONFIG."
  (cdr (sm/goal-position config)))

;; helpers

(defun sm/is-map-edge-p (x y map)
  "Return t is a player is allowed to move to map grid X Y given STATE and MAP."
  (not (and (<= 1 y (sm/map-height map))
            (<= 1 x (sm/map-width map)))))

(defun sm/is-tile-occupied-p (x y map)
  "Return t if tile at X Y in MAP is tile is occupied."
  (member (cons x y) (sm/map-walls map)))

(defun reason-blocked-p (x y state config)
  "Return reason why movement to X Y given STATE CONFIG is not allowed.
Return nil if is allowed."
  (let ((next-x (+ x (sm/player-x state)))
        (next-y (+ y (sm/player-y state))))
    (cond ((sm/is-map-edge-p next-x next-y (sm/map config)) "Something is blocking your passage.")
          ((sm/is-tile-occupied-p next-x next-y (sm/map config)) "You feel cold stone beneath your fingers.")
          (t nil))))

(defun is-goal-p (x y state)
  "Return t when X and Y in are goal in STATE."
  (and (= x (sm/goal-x state))
       (= y (sm/goal-y state))))

(defun reached-goal-p (x y state)
  "Return reason why movement to X Y given STATE CONFIG is not allowed.
Return nil if is allowed."
  (let ((next-x (+ x (sm/player-x state)))
        (next-y (+ y (sm/player-y state))))
    (cond ((is-goal-p next-x next-y state) "There is an exit here.")
          (t nil))))

(defun sm/try-move (x y state config)
  "Move the Character X and Y given STATE and CONFIG."
  (let ((reason-blocked (reason-blocked-p x y state config))
        (reached-goal (reached-goal-p x y state)))
    ;; tile status
    (if reached-goal
        (sm/update-status reached-goal))
    ;; player status
    (cond ((null reason-blocked)
           (if (null reached-goal)
               (sm/update-status "You step lightly in the dark"))
           (move-character x y state config))
          (t
           (sm/update-status (format "You cannot move there. %s" reason-blocked))
           state))))

(defun sm/try-to-win (state config)
  "Try and win the game given STATE and CONFIG."
  (let ()
    (message "trying to win")
    (cond ((is-goal-p (sm/player-x state) (sm/player-y state) state)
           (sm/update-status "You found the exit and descend further!")
           (sm/display-game-over))
          (t
           (sm/update-status "There is not an exit here." )
           state))))

(defun sm/wait-and-process-input ()
  "Wait then call action based on input."
  (let ((key (read-event "Press a key to move.")))
    (cond ((eq key 'left)   (setq *STATE* (sm/try-move -1   0 *STATE* *CONFIG*)))
          ((eq key 'right)  (setq *STATE* (sm/try-move  1   0 *STATE* *CONFIG*)))
          ((eq key 'up)     (setq *STATE* (sm/try-move  0  -1 *STATE* *CONFIG*)))
          ((eq key 'down)   (setq *STATE* (sm/try-move  0   1 *STATE* *CONFIG*)))
          ((eq key ?\>)     (setq *STATE* (sm/try-to-win *STATE* *CONFIG*)))
          ((eq key 'f5)     (let ()
                              (sm/save-state *STATE* "./data/game.dat")
                              (sm/save-config *CONFIG* "./data/game.dat.conf")))
          ((or (eq key ?\q)
               (eq key 'escape)) nil) ;; exit
          (t t))))

(defun sm/draw-char (char x y)
  "Draw CHAR at position X Y given width relative to the current cursor."
  (let ()
    (push-mark)
    (forward-line (1- y))
    (forward-char (1- x))
    (delete-char 1)
    (insert-char char 1)
    (pop-global-mark)))

(defun sm/set-char (x y character)
  "SET position X Y to CHARACTER relative to the current cursor."
  (sm/draw-char character x y))

(defun move-character (x y state config)
  "Move the player by X and Y given STATE and CONFIG."
  (let* ((curr-x (sm/player-x state))
         (curr-y (sm/player-y state))
         (new-x (+ x curr-x))
         (new-y (+ y curr-y)))
    ;; TODO: don't do this for permenant tiles, like the exit
    (sm/set-char curr-x curr-y
                 (if (is-goal-p curr-x curr-y state)
                     (sm/char-goal config)
                   (sm/char-background config)))
    (sm/draw-char (sm/char-player config) new-x new-y)
    (list (cons new-x new-y) (sm/goal-position state))))

;; game functions
(defun sm/initalize-screen (min-height &optional char)
  "Clear and then fill with whitespace a screen with MIN-HEIGHT using CHAR."
  (erase-buffer)
  (if (< (window-body-height) min-height)
      (enlarge-window (- (+ 5 min-height) (window-body-height))))
  (setq screen-height (window-body-height))
  (setq screen-width (window-body-width))
  (dotimes (i screen-height)
    (insert-char (or char ?\#) screen-width)
    (if (> screen-height  (+ 1 i))
        (newline)))
  (goto-char 0))

(defun sm/draw-map (map &optional background offset-x offset-y)
  "Draw the Map display based on MAP and BACKGROUND with OFFSET-X ans OFFSET-Y.
By default the whole map is walkable floor."
  (let ((height (sm/map-height map))
        (width (sm/map-width map))
        (background-char (or background ?\#))
        (offset-x (+ (current-column) (or offset-x 0)))
        (offset-y (+ (line-number-at-pos) (or offset-y 0))))
    (goto-char 0)
    (forward-line (1- offset-y))
    (line-move-to-column offset-x)
    (push-mark)
    (line-move-to-column 1)
    (dotimes (y height)
      (line-move-to-column offset-x)
      (delete-char width)
      (dotimes (x width)
        (if (sm/is-tile-occupied-p (1+ x) (1+ y) map)
            (insert-char ?\# 1)
          (insert-char background-char 1)))
      (forward-line 1))
    (pop-global-mark)))

(defun sm/update-status (message)
  "Display MESSAGE on the final row of the screen."
  (let* ((height screen-height)
        (text (format "Status: %s" message))
        (text-length (length text)))
    (push-mark)
    (forward-line (- height (line-number-at-pos)))
    (kill-line)
    (insert text)
    (pop-global-mark)))

(defun sm/start (cleanup)
  "Start the game, Run CLEANUP on exit."
  (let ((map (sm/map *CONFIG*))
        (status "It is dark and you feel groggy."))
    (sm/initalize-screen (sm/map-height map))
    (sm/draw-map map (sm/char-background *CONFIG*))
    (setq *STATE* (sm/create-random-state map))
    (sm/update-status status)
    (sm/draw-char (sm/char-player *CONFIG*) (sm/player-x *STATE*) (sm/player-y *STATE*))
    (sm/draw-char (sm/char-goal *CONFIG*) (sm/goal-x *STATE*) (sm/goal-y *STATE*))
    (while (not (null (sm/wait-and-process-input))))  ;; (sm/wait-and-process-input) will return nil on escape & q
    (funcall cleanup)))

(defun sm/create-random-state (map)
  "Create a random state based on MAP."
  (let ((goal (cons (random (sm/map-width map)) (random (sm/map-height map))))
        (start (cons (random (sm/map-width map)) (random (sm/map-height map)))))
    (if (or (member goal (sm/map-walls map))
            (member start (sm/map-walls map)))
        (sm/create-random-state map)
      (list start goal))))

(defun sm/display-game-over ()
  "Display the game over screen."
  (let ((map (sm/map *CONFIG*)))
    (sm/initalize-screen (sm/map-height map))
    (animate-string "  You found an exit and stumble out and the sun blinds you.  "
                    (/ (sm/map-height map) 2))))

;; help-me-dev-bindings

(defun debug-with-return (list)
  "Pring debug message for LIST and return it."
  (let () (message (format "%s" list)) list))

(defun sm/run ()
  "Run game."
  (interactive)
  (let ()
    (switch-to-buffer-other-window "game")
    (setq cursor-type nil)
    (sm/start
     (lambda () (setq cursor-type t)))
    (other-window 1)))

(defun sm/build-n-run ()
  "Build and Run."
  (let ()
    (eval-buffer)
    (switch-to-buffer-other-window "game")
    (setq cursor-type nil)
    (sm/start
     (lambda () (setq cursor-type t)))
    (other-window 1)
    ;; (delete-window)
    ))

;; (global-set-key (kbd "<f12>") (lambda () (interactive) (sm/build-n-run)))

(provide 'simple-maze)
;;;  simple-maze.el ends here
