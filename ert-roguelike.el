(ert-delete-all-tests)
(global-set-key (kbd "C-c t") (lambda () (interactive)
                                (load-file "./roguelike.el")
                                (load-file "./ert-roguelike.el")
                                (ert-run-tests-interactively t)))

;; helpers
; https://www.gnu.org/software/emacs/manual/html_node/ert/Fixtures-and-Test-Suites.html
(defun my-fixture (setup teardown body)
  (unwind-protect
      (progn (funcall setup)
             (funcall body))
    (funcall teardown)))

;; config
(ert-deftest test-rl/map-success ()
  (let ()
    (setq config (list (rl/make-map 1 4) (list ?\x ?\x ?\x)))
    (should (equal (rl/map config) (rl/make-map 1 4)))))

(ert-deftest test-rl/is-tile-occupied-p-wall ()
  (let ()
    (should (rl/is-tile-occupied-p 1 1 (rl/make-map 5 5)))))

(ert-deftest test-rl/make-wall ()
  (let ()
    (should (equal (rl/make-wall 1 1 2 1) '((2 . 1) (1 . 1))))))

(ert-deftest test-rl/map-width-success ()
  (let ()
    (should (= (rl/map-width (rl/make-map 1 4)) 1) )))

(ert-deftest test-rl/map-height-success ()
  (let ()
    (should (= (rl/map-height (rl/make-map 1 4)) 4) )))

(ert-deftest test-rl/char-background-success ()
  (let ()
    (setq config '((1 . 4) (?\. ?\@ ?\X)))
    (should (= (rl/char-background config) ?\.) )))

(ert-deftest test-rl/char-player-success ()
  (let ()
    (setq config '((1 . 4) (?\. ?\@ ?\X)))
    (should (= (rl/char-player config) ?\@) )))

(ert-deftest test-rl/char-goal-success ()
  (let ()
    (setq config '((1 . 4) (?\. ?\@ ?\X)))
    (should (= (rl/char-goal config) ?\X) )))

(ert-deftest test-rl/load-map-includes-dimensions ()
  (let ()
    (setq dimensions '(2 . 2))
    (should (equal (car (rl/load-map "./data/2x2.map")) dimensions) )))

(ert-deftest test-rl/load-map-includes-walls ()
  (let ()
    (setq walls '((1 . 2) (1 . 1)) )
    (should (equal walls (cadr (rl/load-map "./data/2x2.map")) ))))

(ert-deftest test-rl/load-state-includes-player ()
  (let ()
    (setq state '((3 . 3)) )
    (should (equal state (rl/load-state "./data/test-state.dat") ))))

(ert-deftest test-rl/save-state-includes-player ()
  (my-fixture
   (lambda () (rl/save-state '((3 . 3)) "./data/test-state-save.dat"))  ; setup
   (lambda () (delete-file "./data/test-state-save.dat"))  ; teardown
   (lambda ()
     (should
      (equal '((3 . 3))
             (rl/load-state "./data/test-state-save.dat") )))))

(provide 'ert-roguelike)
;;;  ert-roguelike.el ends here
