Jan 16 Simple Maze
======
**Simple Maze** is a game a month game or Jan of 2016.

## Download
* [Version 1.0](https://bitbucket.org/sreal/2016-01-simple-map/get/v1.0.zip)

## Usage
```$ git clone git@bitbucket.org:sreal/2016-01-simple-map.git
```

## License
* see [LICENSE](https://github.com/username/sw-name/blob/master/LICENSE.md) file

## Version
* Version 1.0

## Contact
#### Developer
* Twitter: [@sreal](https://twitter.com/sreal "sreal on twitter")

[![Flattr this git repo](http://api.flattr.com/button/flattr-badge-large.png)](https://flattr.com/submit/auto?user_id=sreal&url=https://bitbucket.org/sreal/2016-01-simple-map&title=2016-01-simple-maze&language=emacs-lisp&category=software)
